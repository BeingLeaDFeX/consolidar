import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() salidaTest: EventEmitter<boolean> = new EventEmitter();

  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  controls = {
    username: this.loginForm.controls.username,
    password: this.loginForm.controls.password,
  };

  Errors = {
    username: this.controls.username.hasError('required')
      ? 'Ingresa un usuario válido' : '',
    password: this.controls.password.hasError('required')
      ? 'Ingresa tu clave' : '',
  };

  constructor(
    private fb: FormBuilder,
    //private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    //console.log("oli");
    //this.router.navigate(['/invitado']);
    this.salidaTest.emit(true);
  }

}
