import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './http'; // Servicio

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PersonaComponent } from './persona/persona.component';

const routes: Routes = [
  {
    path: '',
    component: PersonaComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'invitado',
    component: PersonaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
