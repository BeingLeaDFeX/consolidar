import { Injectable } from '@angular/core';
import { MINISTERIOS } from '../data';

@Injectable({
  providedIn: 'root'
})
export class MinisterioService {

  constructor() { }

  getMinisterios(): string[] {
      return MINISTERIOS;
  }
}
