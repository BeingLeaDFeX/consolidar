import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Usuario } from '../http-models';
import { HttpErrorHandlerService,
         HandleError
       } from './http-error-handler.service'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable(
  //{ providedIn: 'root' }
)
export class UsuarioService {
  urls = {
    getUsuarios: 'api/usuarios-issue',
    addUsuario: 'api/usuario-issue',
    buscarUsuario: 'api/usuario-buscar',
    eliminarUsuario: 'api/usuario-delete',
  };
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError('UsuarioService');
  }
/*
  getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.urls.getUsuarios)
    .pipe(
      catchError(this.handleError('UsuarioService.getUsuarios()', []))
    );
  }*/

  addUsuario (usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.urls.addUsuario, usuario, httpOptions)
      .pipe(
        catchError(this.handleError('UsuarioService.addUsuario()', usuario))
      );
  }

  buscarUsuario (usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.urls.buscarUsuario, usuario, httpOptions)
      .pipe(
        catchError(this.handleError('UsuarioService.buscarUsuario()', usuario))
      );
  }
}
