import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './http'; // Servicio

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { InvitadoComponent } from './invitado/invitado.component';

const routes: Routes = [
  {
    path: '',
    component: InvitadoComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'invitado',
    component: InvitadoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
