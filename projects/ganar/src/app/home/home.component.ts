import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeografiaService, MinisterioService,
         Region,
        } from '../data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [
    //HttpMessageService,
    GeografiaService,
    MinisterioService,
  ],
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(

  ) { }

  ngOnInit() {
  
  }

}// The End ...
