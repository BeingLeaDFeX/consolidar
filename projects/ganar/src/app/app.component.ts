import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ganar';
  _loginVista: boolean = true;

  loginVista() {
    this._loginVista = false;
  }
}
