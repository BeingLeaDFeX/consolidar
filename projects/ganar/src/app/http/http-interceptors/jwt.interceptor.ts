import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AutenticacionService } from '../http-services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
              @Inject(PLATFORM_ID) private platformId: Object,
              private autenticacionService: AutenticacionService
            ) { }

  intercept( request: HttpRequest<any>, next: HttpHandler ):
    Observable<HttpEvent<any>>
    {
    // add authorization header with jwt token if available
    if (isPlatformBrowser(this.platformId)) {

      console.log("JwtInterceptor() starts");
      let currentUser = this.autenticacionService.currentUsuarioValue;
      console.log("JwtInterceptor() try{user}:");
      console.log(currentUser);
      /*try {
        console.log("JwtInterceptor() try{user}: "+JSON.stringify(currentUser));
      } catch { console.log("JwtInterceptor(): currentUser don't exist"); }
      try {
        console.log("JwtInterceptor() try{token}: "+currentUser[0].token);
      } catch { console.log("JwtInterceptor(): token don't exist"); }
*/
      try {
      if (currentUser !== undefined && currentUser[0].token !== undefined) {
        console.log("JwtInterceptor(): Por añadir Token!");
        request = request.clone({
                headers: request.headers.set("Authorization",
                    "Bearer " + currentUser[0].token)
            });
/*
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${currentUser.token}`
          }
        });*/
        console.log("JwtInterceptor(): Token añadido!");
        //return next.handle(request);
  /*    }
      else {
            console.log("JwtInterceptor(): don't working");
            return next.handle(request);*/
        }
      } catch { console.log("JwtInterceptor(): Token NO añadido!"); }
      return next.handle(request);
    }
  }
}
