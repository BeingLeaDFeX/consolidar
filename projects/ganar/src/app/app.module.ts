import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';//forms

import {  httpInterceptorProviders,
          AuthGuard,
          AutenticacionService,
          LoginUsuario,
          HttpErrorHandlerService,
          HttpMessageService,
        } from './http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';//forms
import { MatNativeDateModule } from '@angular/material';//forms

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module'; //Contiene componentes material para evitar desordenar aqui
import { LayoutModule } from '@angular/cdk/layout'; //Requerido para funcionar

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { InvitadoComponent } from './invitado/invitado.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    InvitadoComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MaterialModule,
  ],
  exports: [
    MaterialModule
  ],
  providers: [
    HttpErrorHandlerService,
    HttpMessageService,
    httpInterceptorProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
