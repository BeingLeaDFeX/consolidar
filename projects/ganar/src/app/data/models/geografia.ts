export interface Pais {
  nombre: string;
  pais: string[];
  regiones:Region[];
};

export interface Region {
  nombre: string;
  comunas: string[];
};
