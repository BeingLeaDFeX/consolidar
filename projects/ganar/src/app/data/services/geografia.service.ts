import { Injectable } from '@angular/core';
import { Pais,
         Region
        } from '../models'
import {
          REGIONES,
          COMUNAS,
        } from '../data';

@Injectable({ providedIn: 'root' })
export class GeografiaService {

  pais: Pais; //regiones
  regiones: Region[]; //co,unas

  constructor() { }

  getRegiones(): String[] {
        return REGIONES;
  //  for (var i = 0; i < REGIONES.length; i++){
      // look for the entry with a matching `code` value
  }

  getComunas(region: string): string[] {
    try {
      var algo = this.filterRegion(region);
      return algo[0].comunas;
    } catch {
      //var algo = this.filterRegion('Metropolitana');
      //return algo[0].comunas;
      return [] as string[];
    }
  }

  filterRegion(region: string){
    return COMUNAS.filter(
      function(COMUNAS){
        return COMUNAS.region == region;
    });
  }



} // The End
