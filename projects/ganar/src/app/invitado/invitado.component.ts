import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeografiaService, MinisterioService,
         Region,
        } from '../data';

@Component({
  selector: 'app-invitado',
  templateUrl: './invitado.component.html',
  providers: [
    //HttpMessageService,
    GeografiaService,
    MinisterioService,
  ],
  styleUrls: ['./invitado.component.scss']
})
export class InvitadoComponent implements OnInit {
  isLinear = false;
  _form1: FormGroup; _controls1; _errors1;
  _form2: FormGroup; _controls2; _errors2;
  _form3: FormGroup; _controls3; _errors3;

  _sexos: string[] = ['Mujer', 'Hombre'];
  _comunas: string[];
  _ministerios: string[];

  constructor(
    private _fb: FormBuilder,
    private geografiaService: GeografiaService,
    private ministerioService: MinisterioService,
  ) { }

  ngOnInit() {
    this._form1 = this._fb.group({
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      sexo: ['', Validators.required],
      edad: ['', Validators.required],
    });

    this._form2 = this._fb.group({
      direccion: ['', Validators.required],
      comuna: ['', Validators.required],
      obs: ['', Validators.required],
    });

    this._form3 = this._fb.group({
      peticion: [''],
      invitante: [''],
      ministerio: [''],
      consejero: ['', Validators.required],
    });

    this._controls1 = {
      nombres: this._form1.controls.nombres,
      apellidos: this._form1.controls.apellidos,
      sexo: this._form1.controls.sexo,
      edad: this._form1.controls.edad,
    };

    this._controls2 = {
      direccion: this._form2.controls.direccion,
      comuna: this._form2.controls.comuna,
      obs: this._form2.controls.obs,
    };

    this._controls3 = {
      peticion: this._form3.controls.peticion,
      invitante: this._form3.controls.invitante,
      ministerio: this._form3.controls.ministerio,
      consejero: this._form3.controls.consejero
    };

    this._errors1 = {
      nombres: this._form1.controls.nombres.hasError('required')
                  ? 'Necesitamos un nombre' :
                  '',
      apellidos: this._form1.controls.apellidos.hasError('required')
                  ? 'Ingresa su apellido' :
                  '',
      sexo: '',
      edad: '',
    };

    this._errors2 = {
      direccion: '',
      comuna: '',
      obs: '',
    };

    this._errors3 = {
      peticion: '',
      invitante: '',
      ministerio: '',
      consejero: '',
    };

    this._comunas = this.geografiaService.getComunas('Metropolitana');
    this._ministerios = this.ministerioService.getMinisterios();

  }

  getMinisterios() {

  }

}
