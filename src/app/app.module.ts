import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';//forms

import {  httpInterceptorProviders,
          AuthGuard,
          AutenticacionService,
          LoginUsuario,
          HttpErrorHandlerService,
          HttpMessageService,
        } from './http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';//forms
import { MatNativeDateModule } from '@angular/material';//forms

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module'; //Contiene componentes material para evitar desordenar aqui
import { LayoutModule } from '@angular/cdk/layout'; //Requerido para funcionar

import { GanarComponent } from './ganar/ganar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [
    AppComponent,
    GanarComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,

    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [
    HttpErrorHandlerService,
    HttpMessageService,
    httpInterceptorProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
