export class LoginUsuario {
    id: number;
    codigo: string;
    username: string;
    email: string;
    last_login: any;
    nombres:string;
    apellidos: string;
    cliente_nombre: string;
    cliente_codigo: string;
    sitio_nombre:string;
    sitio_codigo: string;
  //  token?: string;
}
