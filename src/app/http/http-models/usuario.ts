export interface Usuario {
  id: number;
  codigo: string;
  username: string;
  password: string;
  email: string;
  last_login: any;
  tipo: string;
  clase: string;
  //
  nombres: string;
  apellidos: string;
  rut: string;
  tel1: string;
  tel2: string;
  correo1: string;
  correo2: string;
  //
  cliente_cod: string;
  cliente_rut: string;
  cliente_nombre: string;
  //
  sitio_cod: string;
  sitio_nombre: string;
  //
  created_on: any;
  existencia: string;
  visible: boolean
  modified: any;
}
