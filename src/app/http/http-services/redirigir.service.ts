import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

//import { LoginUsuario } from '../http-models/login-usuario';
import { HttpErrorHandlerService,
         HandleError
       } from '../http-services/http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class RedirigirService {
  loginUrl = 'api/ti';  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler
      .createHandleError('RedirigirService.getRedir()');
  }

  getRedir (): Observable<any> {
    return this.http.get<any>(this.loginUrl)
      .pipe(
        catchError(this.handleError('RedirigirService.getRedir()', []))
      );
  }
}
