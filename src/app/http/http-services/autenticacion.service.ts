import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from "@angular/common";

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { map } from 'rxjs/operators';

import { LoginUsuario } from '../http-models';
import { HttpErrorHandlerService,
         HandleError
       } from './http-error-handler.service'
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({ providedIn: 'root' })

export class AutenticacionService {

  private currentUsuarioSubject: BehaviorSubject<LoginUsuario>;
  public currentUsuario: Observable<LoginUsuario>;
  private handleError: HandleError;

  loginUrl = 'api/login';
  consultarUrl = 'api/login-issue';

  constructor(
              private http: HttpClient,
              httpErrorHandler: HttpErrorHandlerService,
              @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.handleError = httpErrorHandler.createHandleError('UsuarioService');

    if (isPlatformBrowser(this.platformId)) {
      this.currentUsuarioSubject = new BehaviorSubject<LoginUsuario>(
        JSON.parse(localStorage.getItem('currentUser')));
      this.currentUsuario = this.currentUsuarioSubject.asObservable();

      console.log("AutenticacionService.constructor()"
        +JSON.parse(localStorage.getItem('currentUser')));
    }
  }

  public get currentUsuarioValue(): LoginUsuario {
    if (isPlatformBrowser(this.platformId)) {
      return this.currentUsuarioSubject.value;
    }
  }

  login(_username: string, _password: string) {

    if (isPlatformBrowser(this.platformId)) {
      //console.log("autenticacionService login()");

      return this.http.post<any>(`${this.loginUrl}`, { _username, _password })
        .pipe(map(user => {
          // store Usuario details and jwt token in local storage to keep
          // Usuario logged in between page refreshes
          localStorage.setItem('currentUsuario', JSON.stringify(user));
          this.currentUsuarioSubject.next(user);

          console.log("autenticacionService login() user: "+ JSON.stringify(user));

          return user;
      }));
    }
  }

  consulta(_user: LoginUsuario): Observable<LoginUsuario> {
    return this.http.post<LoginUsuario>(this.consultarUrl, _user, httpOptions)
      .pipe(
        catchError(this.handleError('UsuarioService.addUsuario()', _user))
      );
  }

  logout() {
    if (isPlatformBrowser(this.platformId)) {
      // remove Usuario from local storage to log Usuario out
        localStorage.removeItem('currentUsuario');
      this.currentUsuarioSubject.next(null);
    }
  }
}
