import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanarComponent } from './ganar.component';

describe('GanarComponent', () => {
  let component: GanarComponent;
  let fixture: ComponentFixture<GanarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
